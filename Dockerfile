FROM node:16 as build
WORKDIR /usr/src/app
COPY . /usr/src/app
 
RUN npm install
RUN ls
  
FROM gcr.io/distroless/nodejs:16
COPY --from=build /usr/src/app /app
WORKDIR /app
EXPOSE 3000
CMD [ "app.js"]
